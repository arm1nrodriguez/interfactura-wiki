import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AngularMaterialRoutingModule } from './angular-material-routing.module';
import { AngularMaterialComponent } from './angular-material.component';
import { OpenModalViewComponent } from './open-modal-view/open-modal-view.component';
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import { CorrecionNotasComponent } from './modal-view/correcion-notas/correcion-notas.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatTableModule} from "@angular/material/table";
import {MatCardModule} from "@angular/material/card";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {TableVirtualScrollModule} from "ng-table-virtual-scroll";
import {ScrollingModule} from "@angular/cdk/scrolling";



@NgModule({
  declarations: [
    AngularMaterialComponent,
    OpenModalViewComponent,
    CorrecionNotasComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialRoutingModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatCardModule,
    MatProgressBarModule,
    TableVirtualScrollModule,
    MatTableModule,
    ScrollingModule,


  ]
})
export class AngularMaterialModule { }
