import { Component, OnInit } from '@angular/core';
import { TableVirtualScrollDataSource } from 'ng-table-virtual-scroll';

const ELEMENT_DATA = [
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  },
  {
    type: 'Error en Firma',
    errorNumber: 2,
    correctionNumber: 0,
    action: ' VER ERRORES INDIVIDUALES'
  }

];


@Component({
  selector: 'gdx-correcion-notas',
  templateUrl: './correcion-notas.component.html',
  styleUrls: ['./correcion-notas.component.scss']
})
export class CorrecionNotasComponent implements OnInit {

  displayedColumns: string[] = ['type', 'errorNumber', 'correctionNumber', 'action'];

  dataSource = new TableVirtualScrollDataSource(ELEMENT_DATA);

  constructor() { }

  ngOnInit(): void {

  }

}
