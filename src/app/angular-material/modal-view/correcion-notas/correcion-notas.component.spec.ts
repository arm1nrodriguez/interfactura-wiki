import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorrecionNotasComponent } from './correcion-notas.component';

describe('CorrecionNotasComponent', () => {
  let component: CorrecionNotasComponent;
  let fixture: ComponentFixture<CorrecionNotasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CorrecionNotasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CorrecionNotasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
