import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenModalViewComponent } from './open-modal-view.component';

describe('OpenModalViewComponent', () => {
  let component: OpenModalViewComponent;
  let fixture: ComponentFixture<OpenModalViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpenModalViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenModalViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
