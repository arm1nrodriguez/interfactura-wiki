import { Component, OnInit } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {CorrecionNotasComponent} from "../modal-view/correcion-notas/correcion-notas.component";

@Component({
  selector: 'gdx-open-modal-view',
  templateUrl: './open-modal-view.component.html',
  styleUrls: ['./open-modal-view.component.scss']
})
export class OpenModalViewComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {}

  onOpenModal(){

    const dialogRef = this.dialog.open(CorrecionNotasComponent,{
      width: '800px',
      height: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });

  }

}
